#! /usr/bin/env python3

import streamlit as st
import numpy as np
import pandas as pd

import sqlite3
import os

st.title("Plotting kmer counts on reads")

# setting up some initial variables for data
default_db_path = "./data/VUN_max_dir_CL0260_contig25_boundary_reads.db"
default_kmer_data_path = "./data/reads_on_boundaries_85bp_8mer_counts.npy"

db_path = default_db_path
kmer_data_path = default_kmer_data_path

db = sqlite3.connect(db_path)

db_path = st.sidebar.text_input('Database Path:', default_db_path)

if st.sidebar.button('Submit', key="db_data_button"):
    if os.path.exists(db_path):
        db.close()
        db = sqlite3.connect(db_path)
    else:
        st.write(f'No database found at current path = {db_path}')
        db_path = default_db_path

kmer_data_path = st.sidebar.text_input('Kmer Data Path:', default_kmer_data_path)

if st.sidebar.button('Submit', key="kmer_data_button"):
    if os.path.exists(kmer_data_path):
        # try loading the path in the try, except block
        pass
    else:
        st.write(f'There is no file at {kmer_data_path}')
        kmer_data_path = default_kmer_data_path

try:
    X = st.cache(np.load)(kmer_data_path)
except:
    st.write(f'An error occurred while loading -> {kmer_data_path}')
    kmer_data_path = default_kmer_data_path
    X = st.cache(np.load)(kmer_data_path)

dbc = db.cursor()

get_tables_command = "select name from sqlite_master where type = 'table';"
table_name = dbc.execute(get_tables_command).fetchone()[0]

dbc.execute(f'SELECT * FROM {table_name} ORDER BY id DESC LIMIT 1')
num_reads = dbc.fetchone()[0]
read_num = st.slider("Read ID Number", 1, num_reads, value=1)
st.write(read_num)

read_exe_string = f'SELECT * FROM {table_name} WHERE "id" = ?'
read = dbc.execute(read_exe_string, (read_num,))
_id, _name, _seq, _pos = dbc.fetchone()

st.write(f"\>{_name}\n{_seq[0:50]} ...")
seq_ids = [x[0] for x in dbc.execute('SELECT "seq_id" FROM CL0260_reads').fetchall()]

df = pd.DataFrame(X.T, columns=seq_ids)
st.line_chart(df[_name])


